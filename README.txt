Overview
--------

Xurxo is a theme designed for radio stations websites.

Features
-------

Responsive: Tested in tablets and mobile devices.
SASS: All the css files are generated with sass :)
Many regions: Header,
Superfish Menu, Highlighted,
Banner, Preface One,
Preface Two, Preface Three,
Content, Sidebar First,
Sidebar Second, Post Content,
Bottom One, Bottom Two,
Bottom Three, Bottom Four, Footer.
Requirements
Drupal 7.x, jQuery Update and Responsive Menus.

Sites that use Xurxo Theme
--------------------------

Tell me about your site here <eleonel.basili@gmail.com>

Credits
-------

Developed by Eleonel Basili <eleonelbasili.co.nz>
